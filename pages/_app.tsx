import "../styles/globals.css";
import "@rainbow-me/rainbowkit/styles.css";
import { RainbowKitProvider } from "@rainbow-me/rainbowkit";
import type { AppProps } from "next/app";
import {
  configureChains,
  createConfig,
  useQueryClient,
  WagmiConfig,
  Chain,
} from "wagmi";
import { connectorsForWallets, darkTheme } from "@rainbow-me/rainbowkit";
import {
  injectedWallet,
  walletConnectWallet,
} from "@rainbow-me/rainbowkit/wallets";
import { polygon } from "wagmi/chains";
import { publicProvider } from "wagmi/providers/public";
import { jsonRpcProvider } from "wagmi/providers/jsonRpc";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { PropsWithChildren } from "react";

function getLlamaRPCUrl(chain: Chain): string {
  if (chain.id == 137) {
    return "https://polygon.llamarpc.com";
  }
  if (chain.id == 1) {
    return "https://eth.llamarpc.com";
  }
  throw Error(`Unsupported chain id ${chain.id}`);
}

const { chains, publicClient, webSocketPublicClient } = configureChains(
  [polygon],
  [
    publicProvider(),
    jsonRpcProvider({
      rpc: (chain) => ({
        http: getLlamaRPCUrl(chain),
      }),
    }),
  ],
  { batch: { multicall: true } },
);

const connectors = connectorsForWallets([
  {
    groupName: "Recommended",
    wallets: [
      injectedWallet({ chains }),
      walletConnectWallet({
        projectId: "4b923293c2ea3507e168c005506c3914",
        chains,
      }),
    ],
  },
]);

const wagmiConfig = createConfig({
  autoConnect: true,
  connectors,
  publicClient,
  webSocketPublicClient,
});

function MyQueryClientProvider({ children }: PropsWithChildren) {
  const queryClient = useQueryClient();
  return (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
  );
}

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <WagmiConfig config={wagmiConfig}>
      <MyQueryClientProvider>
        <RainbowKitProvider chains={chains} theme={darkTheme()}>
          <Component {...pageProps} />
        </RainbowKitProvider>
      </MyQueryClientProvider>
    </WagmiConfig>
  );
}

export default MyApp;
