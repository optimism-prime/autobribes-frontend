import { useEffect, useState } from "react";

export const range = (count: number | bigint) =>
  Array.from(new Array(Number(count)).keys());
export const toNumbers = (bigintArray: bigint[]) =>
  bigintArray.map((x) => Number(x));

export const useIsMounted = () => {
  const [mounted, setMounted] = useState(false);

  useEffect(() => setMounted(true), []);

  return mounted;
};
