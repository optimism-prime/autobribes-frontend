import {
  TokenInfo,
  TokenPrice,
  computeApr,
  useALMTokenPrices,
  useActiveLiquidityManagerInfo,
  useTokenPrice,
} from "contracts/pricing";
import {
  Relic,
  ReliquaryInfo,
  ReliquaryPoolAggregatedInfo,
  ReliquaryPoolInfo,
  aggregateReliquaryPoolsInfo,
  useRelicIds,
  useRelics,
  useReliquaryInfo,
  useReliquaryPools,
  useReliquaryPoolsTokens,
} from "contracts/reliquary";
import { PropsWithChildren, createContext, useContext, useMemo } from "react";
import { range } from "utils/utils";
import { Address, formatUnits } from "viem";
import { useToken } from "wagmi";

export const ReliquaryUI = () => {
  const { data: reliquaryInfo } = useReliquaryInfo();
  const { data: rewardToken } = useToken({
    address: reliquaryInfo?.rewardToken,
  });
  const rewardTokenPrice = useTokenPrice(reliquaryInfo?.rewardToken);

  if (!reliquaryInfo || !rewardToken || !rewardTokenPrice) {
    return <div>Loading reliquary...</div>;
  }

  return (
    <>
      <div>
        {rewardToken.symbol} {rewardToken.name}{" "}
        {rewardToken.totalSupply.formatted}
      </div>
      <div>{rewardTokenPrice.formatted}</div>
      <div>{reliquaryInfo.poolLength} pools</div>
      <div>{reliquaryInfo.totalAllocPoint} alloc points</div>
      <div>{reliquaryInfo.totalSupply} relics</div>
      <ReliquaryContextProvider
        reliquaryInfo={reliquaryInfo}
        rewardToken={rewardToken}
        rewardTokenPrice={rewardTokenPrice}
      >
        <Relics />
        <ReliquaryPools />
      </ReliquaryContextProvider>
    </>
  );
};

function isALMToken(address: Address) {
  return true; // In Turbo v1 all Reliquary pool tokens are ALM token but in v2 we'll have TRB
}

function getALMTokens(tokens?: TokenInfo[]): TokenInfo[] {
  return tokens === undefined
    ? []
    : tokens.filter((token) => isALMToken(token.address));
}

type ReliquaryContextProviderProps = {
  reliquaryInfo: ReliquaryInfo;
  rewardToken: TokenInfo;
  rewardTokenPrice: TokenPrice;
};
const ReliquaryContextProvider = ({
  reliquaryInfo,
  rewardToken,
  rewardTokenPrice,
  children,
}: PropsWithChildren<ReliquaryContextProviderProps>) => {
  const { data: pools } = useReliquaryPools(range(reliquaryInfo.poolLength));
  const { data: poolsTokens } = useReliquaryPoolsTokens(pools);
  const { data: almInfo } = useActiveLiquidityManagerInfo(
    getALMTokens(poolsTokens),
  );
  const almTokenPrices = useALMTokenPrices(almInfo ? almInfo : {});

  const rewardRate = parseFloat(
    formatUnits(reliquaryInfo.rewardRate, rewardToken.decimals),
  );
  const rewardRateUsd = rewardRate * rewardTokenPrice.value;

  const poolsAggregatedInfo = useMemo(
    () =>
      pools &&
      poolsTokens &&
      almTokenPrices &&
      aggregateReliquaryPoolsInfo(
        pools,
        poolsTokens,
        almTokenPrices,
        reliquaryInfo.totalAllocPoint,
      ),
    [pools, poolsTokens, almTokenPrices, reliquaryInfo],
  );

  const aggregatedInfo = useMemo(() => {
    if (poolsAggregatedInfo === undefined) {
      return undefined;
    }
    return {
      rewardRate,
      rewardRateUsd,
      tvlUsd: poolsAggregatedInfo
        .map((poolInfo) => poolInfo.balanceUsd || 0)
        .reduce((sum, current) => sum + current, 0),
    };
  }, [poolsAggregatedInfo, rewardRate, rewardRateUsd]);

  if (
    !pools ||
    !poolsTokens ||
    !almInfo ||
    !almTokenPrices ||
    !poolsAggregatedInfo ||
    !aggregatedInfo
  ) {
    return <></>;
  }

  const context = {
    reliquaryInfo,
    rewardToken,
    rewardTokenPrice,
    pools,
    poolsTokens,
    poolsTokenPrices: almTokenPrices,
    poolsAggregatedInfo,
    aggregatedInfo,
  };
  return (
    <ReliquaryContext.Provider value={context}>
      {children}
    </ReliquaryContext.Provider>
  );
};

type Reliquary = {
  reliquaryInfo: ReliquaryInfo;
  rewardToken: TokenInfo;
  rewardTokenPrice: TokenPrice;
  pools: ReliquaryPoolInfo[];
  poolsTokens: TokenInfo[];
  poolsTokenPrices: { [key: Address]: TokenPrice | undefined };
  poolsAggregatedInfo: ReliquaryPoolAggregatedInfo[];
  aggregatedInfo: AggregatedReliquaryInfo;
};

type AggregatedReliquaryInfo = {
  rewardRate: number;
  rewardRateUsd: number;
  tvlUsd: number;
};

export const ReliquaryContext = createContext({} as Reliquary);
export const useReliquary = () => {
  return useContext(ReliquaryContext);
};

const Relics = () => {
  const { reliquaryInfo } = useReliquary();
  const { data: relicIds } = useRelicIds(reliquaryInfo.totalSupply);
  return relicIds && <RelicsFromIds relicIds={relicIds} />;
};

const RelicsFromIds = ({ relicIds }: { relicIds: number[] }) => {
  const { data: relics } = useRelics(relicIds);
  return (
    relics && (
      <div>
        {relics.map((relic) => (
          <Relic key={relic.id} relic={relic} />
        ))}
      </div>
    )
  );
};

const Relic = ({ relic }: { relic: Relic }) => {
  const { rewardToken, poolsTokens, poolsTokenPrices } =
    useContext(ReliquaryContext);
  const token = poolsTokens[relic.position.poolId];
  const amount = formatUnits(relic.position.amount, token.decimals);
  const tokenPrice = poolsTokenPrices[token.address];
  const tvl = tokenPrice && tokenPrice.value * parseFloat(amount);
  return (
    <div>
      <h1>Relic {relic.id}</h1>
      <ul className="flex flex-col list-disc pl-10">
        <li>owner: {relic.owner}</li>
        <li>
          pendingReward:{" "}
          {formatUnits(relic.pendingReward, rewardToken.decimals)}
        </li>
        <li>levelOnUpdate: {relic.levelOnUpdate}</li>
        <li>amount: {amount}</li>
        <li>tvl: ${tvl}</li>
        <li>entry: {relic.position.entry}</li>
        <li>level: {relic.position.level}</li>
        <li>poolId: {relic.position.poolId}</li>
        <li>
          rewardCredit:{" "}
          {formatUnits(relic.position.rewardCredit, rewardToken.decimals)}
        </li>
        <li>
          rewardDebt:{" "}
          {formatUnits(relic.position.rewardDebt, rewardToken.decimals)}
        </li>
      </ul>
    </div>
  );
};

const ReliquaryPools = () => {
  const {
    pools,
    poolsTokens,
    poolsTokenPrices,
    poolsAggregatedInfo,
    aggregatedInfo,
  } = useReliquary();

  return (
    <div>
      <div>
        APR:{" "}
        {computeApr(aggregatedInfo.tvlUsd, aggregatedInfo.rewardRateUsd, 1.0)}
      </div>
      {pools
        .map((pool, poolIndex) => ({
          pool,
          poolAggregatedInfo: poolsAggregatedInfo[poolIndex],
          tokenInfo: poolsTokens[poolIndex],
          poolTokenPrice: poolsTokenPrices[pool.token],
        }))
        .map(
          (
            { pool, poolAggregatedInfo, tokenInfo, poolTokenPrice },
            poolIndex,
          ) => (
            <div key={poolIndex} className="flex flex-col">
              <p>Pool {poolIndex}</p>
              <ul className="flex flex-col list-disc pl-10">
                <li>
                  Token: {pool.token} {tokenInfo.name} {tokenInfo.symbol}{" "}
                  {tokenInfo.decimals} {tokenInfo.totalSupply.formatted}
                </li>
                <li>
                  Balance:{" "}
                  {formatUnits(poolAggregatedInfo.balance, tokenInfo.decimals)}
                </li>
                {/* <li>Liquidity Pool: {almInfo.liquidityPool}</li>
              <li>Token 0: {almInfo.token0}</li>
              <li>Token 1: {almInfo.token1}</li> */}
                <li>
                  Token Price: {poolTokenPrice && poolTokenPrice.formatted}
                </li>
                <li>Balance: {poolAggregatedInfo.formattedBalance}</li>
                <li>TVL: ${poolAggregatedInfo.balanceUsd}</li>
                <li>Alloc points: {pool.allocPoint}</li>
                <li>Reward Share: {poolAggregatedInfo.rewardShare}</li>
                <li>
                  APR:{" "}
                  {poolAggregatedInfo.balanceUsd &&
                    computeApr(
                      poolAggregatedInfo.balanceUsd,
                      aggregatedInfo.rewardRateUsd,
                      poolAggregatedInfo.rewardShare,
                    )}
                </li>
                {/* <li>Amounts: {almInfo.totalAmounts}</li> */}
                {poolAggregatedInfo.levelsInfo
                  .filter((levelInfo) => levelInfo.balanceUsd != 0)
                  .map((levelInfo, levelIdx) => {
                    return (
                      <div key={levelIdx}>
                        <p>Level {levelIdx}</p>
                        <ul className="flex flex-col list-disc pl-10">
                          <li>TVL: ${levelInfo.balanceUsd}</li>
                          <li>Reward share: {levelInfo.absoluteRewardShare}</li>
                          <li>
                            APR:{" "}
                            {levelInfo.balanceUsd &&
                              computeApr(
                                levelInfo.balanceUsd,
                                aggregatedInfo.rewardRateUsd,
                                levelInfo.absoluteRewardShare,
                              )}
                          </li>
                        </ul>
                      </div>
                    );
                  })}
              </ul>
            </div>
          ),
        )}
    </div>
  );
};
