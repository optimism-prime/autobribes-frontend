import Notification from "components/Notification";
import { WriteContractResult } from "wagmi/dist/actions";
import { useBlockExplorerUrl } from "./hooks/useBlockExplorerUrl";
import { Chain, ReplacementReturnType, TransactionReceipt } from "viem";

export function useContractWriteEvents(txName: string) {
  const blockExplorerUrl = useBlockExplorerUrl();
  return {
    onError(error: Error) {
      Notification({
        type: "error",
        title: "Transaction Error",
        message: error.message,
        link: "",
      });
    },
    onSuccess(data: WriteContractResult) {
      Notification({
        type: "",
        title: "Transaction Submitted",
        message: `Your ${txName} transaction was successfully submitted.`,
        link: `${blockExplorerUrl}/tx/${data.hash}`,
      });
    },
  };
}

export function useWaitForTransactionEvents(txName: string) {
  const blockExplorerUrl = useBlockExplorerUrl();
  return {
    onSuccess(data: TransactionReceipt) {
      Notification({
        type: "success",
        title: "Transaction Confirmed",
        message: `${txName} confirmed in block ${data.blockNumber}`,
        link: `${blockExplorerUrl}/tx/${data.transactionHash}`,
      });
    },
    onReplaced(data: ReplacementReturnType<Chain | undefined>) {
      Notification({
        type: "success",
        title: "Transaction Replaced",
        message: `Your ${txName} transaction was successfully replaced.`,
        link: `${blockExplorerUrl}/tx/${data.transaction.hash}`,
      });
    },
    onError(error: Error) {
      Notification({
        type: "error",
        title: "Transaction Error",
        message: error.message,
        link: "",
      });
    },
  };
}
