import { Address, formatUnits, getAddress } from "viem";
import {
  useContractRead,
  useContractReads,
  useContractWrite,
  usePrepareContractWrite,
  useWaitForTransaction,
} from "wagmi";
import { ReliquaryEmissionCurve } from "./abi/ReliquaryEmissionCurve";
import { AUTOBRIBES_RETRO_CONTRACT } from "./constants";
import { range, toNumbers } from "utils/utils";
import { useMemo } from "react";
import { ERC20 } from "./abi/ERC20";
import { TokenInfo, TokenPrice } from "./pricing";
import {
  useContractWriteEvents,
  useWaitForTransactionEvents,
} from "./notification";

export type ReliquaryInfo = {
  address: Address;
  poolLength: number;
  totalAllocPoint: number;
  totalSupply: number;
  rewardToken: Address;
  rewardRate: bigint;
  rewardBalance: bigint;
};

export type UseReliquaryInfoResult = {
  data?: ReliquaryInfo;
} & UseQueryResult;

export function useReliquaryInfo(): UseReliquaryInfoResult {
  const { data: fetchedData, ...queryResult } = useContractReads({
    contracts: [
      {
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "totalAllocPoint",
      },
      {
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "poolLength",
      },
      {
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "totalSupply",
      },
      {
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "rewardToken",
      },
      {
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "emissionCurve",
      },
    ],
    allowFailure: false,
  });
  const { data: rewardRate, ...rewardRateQueryResult } = useContractRead({
    enabled: fetchedData !== undefined,
    address: fetchedData && fetchedData[4],
    abi: ReliquaryEmissionCurve,
    functionName: "getRate",
    args: [0n],
  });
  const { data: rewardBalance, ...rewardBalanceQueryResult } = useContractRead({
    enabled: fetchedData !== undefined,
    address: fetchedData && fetchedData[3],
    abi: ERC20,
    functionName: "balanceOf",
    args: [AUTOBRIBES_RETRO_CONTRACT.address],
  });

  if (fetchedData === undefined) {
    return { ...queryResult };
  }
  if (rewardRate === undefined) {
    return { ...rewardRateQueryResult };
  }
  if (rewardBalance === undefined) {
    return { ...rewardBalanceQueryResult };
  }
  const [totalAllocPoint, poolLength, totalSupply, rewardToken] = fetchedData;

  const data = {
    address: AUTOBRIBES_RETRO_CONTRACT.address,
    totalAllocPoint: Number(totalAllocPoint),
    poolLength: Number(poolLength),
    totalSupply: Number(totalSupply),
    rewardToken,
    rewardRate,
    rewardBalance,
  };

  return {
    ...queryResult,
    data,
  };
}

export type UseRelicIdsResult = {
  data?: number[];
} & UseQueryResult;

export function useRelicIds(totalSupply: number): UseRelicIdsResult {
  const { data, ...queryResult } = useContractReads({
    allowFailure: false,
    contracts: range(totalSupply).map((tokenIdx) => ({
      ...AUTOBRIBES_RETRO_CONTRACT,
      functionName: "tokenByIndex",
      args: [tokenIdx],
    })),
  });

  if (data === undefined) {
    return { ...queryResult };
  }

  return {
    ...queryResult,
    data: toNumbers(data as bigint[]),
  };
}

export type RelicPosition = {
  amount: bigint;
  rewardDebt: bigint;
  rewardCredit: bigint;
  entry: number;
  poolId: number;
  level: number;
};

export type Relic = {
  id: number;
  position: RelicPosition;
  pendingReward: bigint;
  levelOnUpdate: number;
  owner: Address;
};

export type UseRelicsResult = {
  data?: Relic[];
} & UseQueryResult;

export function useRelics(relicIds: number[]): UseRelicsResult {
  const { data: fetchedData, ...queryResult } = useContractReads({
    allowFailure: false,
    contracts: relicIds
      .map((tokenId) => ({
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "getPositionForId",
        args: [tokenId],
      }))
      .concat(
        relicIds.map((tokenId) => ({
          ...AUTOBRIBES_RETRO_CONTRACT,
          functionName: "pendingReward",
          args: [tokenId],
        })),
      )
      .concat(
        relicIds.map((tokenId) => ({
          ...AUTOBRIBES_RETRO_CONTRACT,
          functionName: "levelOnUpdate",
          args: [tokenId],
        })),
      )
      .concat(
        relicIds.map((tokenId) => ({
          ...AUTOBRIBES_RETRO_CONTRACT,
          functionName: "ownerOf",
          args: [tokenId],
        })),
      ),
  });
  if (fetchedData === undefined) {
    return { ...queryResult };
  }

  const data = relicIds.map((relicId, relicIdx) => {
    const position = fetchedData[relicIdx] as any;
    const pendingReward = fetchedData[relicIds.length + relicIdx] as bigint;
    const levelOnUpdate = fetchedData[2 * relicIds.length + relicIdx] as bigint;
    const owner = fetchedData[3 * relicIds.length + relicIdx] as Address;

    return {
      id: relicId,
      position: {
        ...position,
        entry: Number(position.entry),
        poolId: Number(position.poolId),
        level: Number(position.level),
      },
      pendingReward,
      levelOnUpdate: Number(levelOnUpdate),
      owner,
    };
  });

  return {
    ...queryResult,
    data,
  };
}

export type UseOwnerRelicIdsResult = {
  data?: number[];
} & UseQueryResult;

export function useOwnerRelicIds(owner: Address): UseOwnerRelicIdsResult {
  const balanceOfResult = useContractRead({
    ...AUTOBRIBES_RETRO_CONTRACT,
    functionName: "balanceOf",
    args: [owner],
  });
  const balanceOf = balanceOfResult.data || 0;

  const tokenOfOwnerByIndexResult = useContractReads({
    allowFailure: false,
    enabled: balanceOfResult.data !== undefined,
    contracts: range(balanceOf).map((tokenIdx) => ({
      ...AUTOBRIBES_RETRO_CONTRACT,
      functionName: "tokenOfOwnerByIndex",
      args: [owner, tokenIdx],
    })),
  });

  if (tokenOfOwnerByIndexResult.data === undefined) {
    return { ...tokenOfOwnerByIndexResult, data: undefined };
  }

  const data = toNumbers(tokenOfOwnerByIndexResult.data as bigint[]);

  return {
    ...tokenOfOwnerByIndexResult,
    data,
  };
}

export type LevelInfo = {
  requiredMaturity: number;
  multiplier: number;
  balance: bigint;
};

export type ReliquaryPoolInfo = {
  accRewardPerShare: bigint;
  lastRewardTime: number;
  allocPoint: number;
  name: string;
  allowPartialWithdrawals: boolean;
  token: Address;
  levelsInfo: LevelInfo[];
};

export type UseReliquaryPoolsResult = {
  data?: ReliquaryPoolInfo[];
} & UseQueryResult;

export function useReliquaryPools(poolIds: number[]): UseReliquaryPoolsResult {
  const { data: fetchedData, ...queryResult } = useContractReads({
    allowFailure: false,
    contracts: poolIds
      .map((poolId) => ({
        ...AUTOBRIBES_RETRO_CONTRACT,
        functionName: "getPoolInfo",
        args: [poolId],
      }))
      .concat(
        poolIds.map((poolId) => ({
          ...AUTOBRIBES_RETRO_CONTRACT,
          functionName: "poolToken",
          args: [poolId],
        })),
      )
      .concat(
        poolIds.map((poolId) => ({
          ...AUTOBRIBES_RETRO_CONTRACT,
          functionName: "getLevelInfo",
          args: [poolId],
        })),
      ),
  });
  const data = useMemo(
    () =>
      fetchedData &&
      poolIds.map((_poolId, poolIndex) => {
        const poolInfo = fetchedData[poolIndex] as any;
        const poolToken = fetchedData[poolIds.length + poolIndex] as Address;
        const levelInfo = fetchedData[2 * poolIds.length + poolIndex] as {
          requiredMaturities: bigint[];
          multipliers: bigint[];
          balance: bigint[];
        };

        return {
          ...poolInfo,
          lastRewardTime: Number(poolInfo.lastRewardTime),
          allocPoint: Number(poolInfo.allocPoint),
          token: poolToken,
          levelsInfo: levelInfo.requiredMaturities.map(
            (requiredMaturity, levelIndex) => ({
              requiredMaturity: Number(requiredMaturity),
              multiplier: Number(levelInfo.multipliers[levelIndex]),
              balance: levelInfo.balance[levelIndex],
            }),
          ),
        };
      }),
    [poolIds, fetchedData],
  );

  return {
    ...queryResult,
    data,
  };
}

export type UseReliquaryPoolResult = {
  data?: ReliquaryPoolInfo;
} & UseQueryResult;

export function useReliquaryPool(poolId: number): UseReliquaryPoolResult {
  const { data, ...queryResult } = useReliquaryPools([poolId]);
  return {
    ...queryResult,
    data: data && data[0],
  };
}

export type ReliquaryPoolLevelAggregatedInfo = {
  formattedBalance: string;
  balanceUsd: number | undefined;
  absoluteRewardShare: number;
  relativeRewardShare: number;
};

export type ReliquaryPoolAggregatedInfo = {
  balance: bigint; // Total balance accross all levels
  formattedBalance: string;
  balanceUsd: number | undefined;
  levelsInfo: ReliquaryPoolLevelAggregatedInfo[];
  rewardShare: number;
};

export function aggregateReliquaryPoolsInfo(
  pools: ReliquaryPoolInfo[],
  poolsTokens: TokenInfo[],
  poolsTokensPrices: { [key: Address]: TokenPrice | undefined },
  totalAllocPoint: number,
): ReliquaryPoolAggregatedInfo[] {
  return pools.map((pool, poolIndex) => {
    const token = poolsTokens[poolIndex];
    const tokenPrice = poolsTokensPrices[token.address];

    const poolRewardShare = pool.allocPoint / totalAllocPoint;

    const weightedBalances = pool.levelsInfo.map((levelInfo) =>
      parseFloat(
        formatUnits(
          levelInfo.balance * BigInt(levelInfo.multiplier),
          token.decimals,
        ),
      ),
    );
    const weightedTotalBalance = weightedBalances.reduce(
      (sum, current) => sum + current,
      0,
    );
    const shares = weightedBalances.map(
      (weightedBalance) => weightedBalance / weightedTotalBalance,
    );

    const levelsInfo = pool.levelsInfo.map((levelInfo, levelIndex) => {
      const formattedBalance = formatUnits(levelInfo.balance, token.decimals);
      const balance = parseFloat(formattedBalance);
      const balanceUsd = tokenPrice && tokenPrice.value * balance;
      const relativeRewardShare = shares[levelIndex];
      const absoluteRewardShare = relativeRewardShare * poolRewardShare;
      return {
        formattedBalance,
        balanceUsd,
        relativeRewardShare,
        absoluteRewardShare,
      };
    });
    const balance = pool.levelsInfo
      .map((levelInfo) => levelInfo.balance)
      .reduce((sum, current) => sum + current, 0n);
    const formattedBalance = formatUnits(balance, token.decimals);
    const balanceAsNumber = parseFloat(formattedBalance);
    const balanceUsd = tokenPrice && tokenPrice.value * balanceAsNumber;
    return {
      balance,
      formattedBalance,
      balanceUsd,
      levelsInfo,
      rewardShare: poolRewardShare,
    };
  });
}

export type UseReliquaryPoolsTokens = {
  data?: TokenInfo[];
} & UseQueryResult;

export function useReliquaryPoolsTokens(
  pools?: ReliquaryPoolInfo[],
): UseReliquaryPoolsTokens {
  const { data, ...queryResult } = useContractReads({
    enabled: pools !== undefined,
    allowFailure: false,
    contracts: pools
      ?.map(({ token }) => ({
        address: token,
        abi: ERC20,
        functionName: "decimals",
      }))
      .concat(
        pools?.map(({ token }) => ({
          address: token,
          abi: ERC20,
          functionName: "name",
        })),
      )
      .concat(
        pools?.map(({ token }) => ({
          address: token,
          abi: ERC20,
          functionName: "symbol",
        })),
      )
      .concat(
        pools?.map(({ token }) => ({
          address: token,
          abi: ERC20,
          functionName: "totalSupply",
        })),
      ),
  });
  if (data === undefined || pools === undefined) {
    return { ...queryResult };
  }
  return {
    ...queryResult,
    data: pools.map(({ token }, poolIdx) => {
      const decimals = Number(data[poolIdx] as bigint);
      const name = data[pools.length + poolIdx] as string;
      const symbol = data[2 * pools.length + poolIdx] as string;
      const totalSupply = data[3 * pools.length + poolIdx] as bigint;

      return {
        address: token,
        decimals: decimals,
        name,
        symbol,
        totalSupply: {
          value: totalSupply,
          formatted: formatUnits(totalSupply, decimals),
        },
      };
    }),
  };
}

type UseTokenAmountResult = {
  data: bigint | undefined;
} & UseQueryResult;

export function useERC20BalanceOf(
  tokenAddress?: Address,
  ownerAddress?: Address,
): UseTokenAmountResult {
  return useContractRead({
    enabled: Boolean(tokenAddress && ownerAddress),
    address: tokenAddress,
    abi: ERC20,
    functionName: "balanceOf",
    args: [ownerAddress!],
  });
}

export function useERC20Allowance(
  tokenAddress?: Address,
  ownerAddress?: Address,
  spenderAddress?: Address,
): UseTokenAmountResult {
  return useContractRead({
    enabled: Boolean(tokenAddress && ownerAddress && spenderAddress),
    address: tokenAddress,
    abi: ERC20,
    functionName: "allowance",
    args: [ownerAddress!, spenderAddress!],
  });
}

type OwnerAggregatedReliquaryInfo = {
  balanceUsd: number;
  perRelicBalanceUsd: number[];
  pendingReward: bigint;
  pendingRewardUsd: number;
  perPoolBalanceUsd: number[];
};

export function aggregateOwnerReliquaryInfo(
  ownerRelics: Relic[],
  rewardToken: TokenInfo,
  rewardTokenPrice: TokenPrice,
  poolsTokens: TokenInfo[],
  poolsTokenPrices: { [key: Address]: TokenPrice | undefined },
): OwnerAggregatedReliquaryInfo {
  const pendingReward = ownerRelics
    .map((relic) => relic.pendingReward)
    .reduce((previousValue, currentValue) => previousValue + currentValue, 0n);
  const pendingRewardUsd =
    parseFloat(formatUnits(pendingReward, rewardToken.decimals)) *
    rewardTokenPrice.value;

  const perPoolBalanceUsd = range(poolsTokens.length).map((idx) => 0);
  let balanceUsd = 0;

  const perRelicBalanceUsd = ownerRelics.map((relic) => {
    const tokenInfo = poolsTokens[relic.position.poolId];
    const tokenPrice = poolsTokenPrices[tokenInfo.address];
    const relicBalanceUsd = tokenPrice
      ? tokenPrice.value *
        parseFloat(formatUnits(relic.position.amount, tokenInfo.decimals))
      : NaN;
    perPoolBalanceUsd[relic.position.poolId] += !Number.isNaN(relicBalanceUsd)
      ? relicBalanceUsd
      : 0;
    balanceUsd += !Number.isNaN(relicBalanceUsd) ? relicBalanceUsd : 0;
    return relicBalanceUsd;
  });

  return {
    balanceUsd,
    perRelicBalanceUsd,
    pendingReward,
    pendingRewardUsd,
    perPoolBalanceUsd,
  };
}

export function useWithdrawFromRelic(relicId?: number, amount?: bigint) {
  const { config } = usePrepareContractWrite({
    enabled: relicId !== undefined && amount !== undefined,
    ...AUTOBRIBES_RETRO_CONTRACT,
    functionName: "withdraw",
    args: [
      amount !== undefined ? amount : 0n,
      relicId !== undefined ? BigInt(relicId) : 0n,
    ],
  });
  const txName = `withdraw from relic ${relicId}`;
  const contractWriteEvents = useContractWriteEvents(txName);
  const writeResult = useContractWrite({
    ...config,
    ...contractWriteEvents,
  });
  const waitForTransactionEvents = useWaitForTransactionEvents(txName);
  const transactionResult = useWaitForTransaction({
    hash: writeResult.data?.hash,
    ...waitForTransactionEvents,
  });
  return {
    txName,
    writeResult,
    transactionResult,
  };
}

export function useDepositInRelic(relicId?: number, amount?: bigint) {
  const { config } = usePrepareContractWrite({
    enabled: relicId !== undefined && amount !== undefined,
    ...AUTOBRIBES_RETRO_CONTRACT,
    functionName: "deposit",
    args: [
      amount !== undefined ? amount : 0n,
      relicId !== undefined ? BigInt(relicId) : 0n,
    ],
  });
  const txName = `deposit in relic ${relicId}`;
  const contractWriteEvents = useContractWriteEvents(txName);
  const writeResult = useContractWrite({
    ...config,
    ...contractWriteEvents,
  });
  const waitForTransactionEvents = useWaitForTransactionEvents(txName);
  const transactionResult = useWaitForTransaction({
    hash: writeResult.data?.hash,
    ...waitForTransactionEvents,
  });
  return {
    txName,
    writeResult,
    transactionResult,
  };
}

export function useCreateRelicAndDeposit(
  recipient?: Address,
  poolId?: number,
  amount?: bigint,
) {
  const { config } = usePrepareContractWrite({
    enabled:
      recipient !== undefined && poolId !== undefined && amount !== undefined,
    ...AUTOBRIBES_RETRO_CONTRACT,
    functionName: "createRelicAndDeposit",
    args: [
      recipient !== undefined ? recipient : getAddress("0x0"),
      poolId !== undefined ? BigInt(poolId) : 0n,
      amount !== undefined ? amount : 0n,
    ],
  });
  const txName = `create relic on pool ${poolId} and deposit`;
  const contractWriteEvents = useContractWriteEvents(txName);
  const writeResult = useContractWrite({
    ...config,
    ...contractWriteEvents,
  });
  const waitForTransactionEvents = useWaitForTransactionEvents(txName);
  const transactionResult = useWaitForTransaction({
    hash: writeResult.data?.hash,
    ...waitForTransactionEvents,
  });
  return {
    txName,
    writeResult,
    transactionResult,
  };
}

export function useHarvestRelic(relicId?: number, recipient?: Address) {
  const { config } = usePrepareContractWrite({
    enabled: relicId !== undefined && recipient !== undefined,
    ...AUTOBRIBES_RETRO_CONTRACT,
    functionName: "harvest",
    args: [
      relicId !== undefined ? BigInt(relicId) : 0n,
      recipient !== undefined ? recipient : getAddress("0x0"),
    ],
  });
  const txName = `harvest relic ${relicId} for recipient ${recipient}`;
  const contractWriteEvents = useContractWriteEvents(txName);
  const writeResult = useContractWrite({
    ...config,
    ...contractWriteEvents,
  });
  const waitForTransactionEvents = useWaitForTransactionEvents(txName);
  const transactionResult = useWaitForTransaction({
    hash: writeResult.data?.hash,
    ...waitForTransactionEvents,
  });
  return {
    txName,
    writeResult,
    transactionResult,
  };
}

export function useUpdateRelicPosition(relicId?: number) {
  const { config } = usePrepareContractWrite({
    enabled: relicId !== undefined,
    ...AUTOBRIBES_RETRO_CONTRACT,
    functionName: "updatePosition",
    args: [relicId !== undefined ? BigInt(relicId) : 0n],
  });
  const txName = `update position of relic ${relicId}`;
  const contractWriteEvents = useContractWriteEvents(txName);
  const writeResult = useContractWrite({
    ...config,
    ...contractWriteEvents,
  });
  const waitForTransactionEvents = useWaitForTransactionEvents(txName);
  const transactionResult = useWaitForTransaction({
    hash: writeResult.data?.hash,
    ...waitForTransactionEvents,
  });
  return {
    txName,
    writeResult,
    transactionResult,
  };
}

export function useERC20Approve(
  tokenAddress?: Address,
  spenderAddress?: Address,
  amount?: bigint,
) {
  const { config } = usePrepareContractWrite({
    enabled:
      tokenAddress !== undefined &&
      spenderAddress !== undefined &&
      amount !== undefined,
    address: tokenAddress,
    abi: ERC20,
    functionName: "approve",
    args: [
      spenderAddress !== undefined ? spenderAddress : getAddress("0x0"),
      amount !== undefined ? amount : 0n,
    ],
  });
  const txName = `allow ${spenderAddress} to spend ${amount} of ${tokenAddress}`;
  const contractWriteEvents = useContractWriteEvents(txName);
  const writeResult = useContractWrite({
    ...config,
    ...contractWriteEvents,
  });
  const waitForTransactionEvents = useWaitForTransactionEvents(txName);
  const transactionResult = useWaitForTransaction({
    hash: writeResult.data?.hash,
    ...waitForTransactionEvents,
  });
  return {
    txName,
    writeResult,
    transactionResult,
  };
}
